# -*- coding: utf-8 -*-
{
    'name': "Arief Afandy for Test Agro Boga",

    'summary': """
        Modul ini untuk Persyaratan Test Odoo Developer""",

    'description': """
        Soal Test:
        1. Jika sudah memilih order line, maka product tidak boleh terisi di line selanjutnya
        2. jika memilih product pada order line, ketiki di deliver maka akan terkirm komponen penyusunnya
    """,

    'author': "Arief Afandy 085726822223",
    'website': "arfan.jobmail@gmail.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': 'ODOO 14',

    # any module necessary for this one to work correctly
    'depends': ['sale','mrp'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        # 'views/sale_order_view.xml',
    ],
    # only loaded in demonstration mode

}
