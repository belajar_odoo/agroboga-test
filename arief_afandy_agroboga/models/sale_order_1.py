# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.constrains('order_line')
    def _check_use_product(self):
      for sale in self:
          use_product = []
          for line in sale.order_line:
             if line.product_id.id in use_product:
                raise ValidationError(_('Hanya Boleh 1 Product'))
             use_product.append(line.product_id.id)



    '''
    Deskripsi:
    1. menggunakan bantuan api.constraint
    2. Ketika memilih Product A, kemudian memilih product A lagi dan save maka akan muncul warning

    Target Pengembangan Selanjutnya:
    1. Membuat fungsi use_product
    2. Mencari semua product pada line
    3. Memasukan pada list product yang sudah digunakan
    4. Mengeluarkan hasil list tersebut
    5. melakukan super pada field product_id bawaan odoo
    6. menambahkan domain berdasarkan fungsi use_product
    7. maka ketika memilih product many2one pada view, product yg sudah di isi tidak muncul karena domain

    '''

