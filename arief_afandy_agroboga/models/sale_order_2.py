# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def create_picking_material(self):        
        for sale in self:
            for line in sale.order_line:
                if line.product_id.bom_ids:
                    bom = self.env['mrp.bom'].search([
                        ('product_id','=',line.product_id.id)],limit=1)
                    
                    for bom_line in bom.bom_line_ids:
                        locations = self.env['stock.quant'].search([
                            ('product_id','=',bom_line.product_id.id),
                            ('inventory_quantity','>=',1)],limit=1)
                        
                        if locations.location_id:
                            pick = sale.env['stock.picking'].create({
                                'partner_id':sale.partner_id.id,
                                'origin':sale.name,
                                'picking_type_id': 2,
                                'location_id': locations.location_id.id,
                                'location_dest_id':5,
                                'move_ids_without_package': [(0, 0, {
                                    'name': bom_line.product_id.name,
                                    'product_id' : bom_line.product_id.id,
                                    'product_uom_qty':bom_line.product_qty,
                                    'product_uom':1,

                                    })]
                                })

    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        for sale in self:
            sale.create_picking_material()
        return res


    '''
    Deskripsi:
    1. Buat Bill of Materials pada Manufacture Pada product A
    2. Product A terdiri dari komponen B, C, D
    3. Atur lokasi produk komponen B, C, D
    4. Persiapan sudah selesai, kita kemudian membuat SO dan mengisi product A
    5. Ketika kita confirm SO, akan terbuat Picking dan akan terbuat picking 
    berdasarkan komponen penyusun yaitu B, C, D sesuai lokasinya
    6. Untuk picking dapat dilihat di Inventory > Transfer > search Origin berdasarkan nomor SO


    Terget pengembangan selanjutnya:
    1. Produk A yang dipilih tidak terbuat di picking ketika confirm SO
    2. Product B, C, D akan muncul pada button action_view_delivery. Karena pada pengembangan ini
    masih belum muncul

    '''
